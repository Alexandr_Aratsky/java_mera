package myclasses;
// класс человека
public class THuman {
	private int destinationFloor; // этаж на который он должен попасть

	
	public THuman(int destination)
	{ 
		destinationFloor = destination;
	}
	public int getDestination()
	{ return destinationFloor;  }
	public String getString()
	{
		return String.format("{%d}", destinationFloor);
	}

}
