package myclasses;

import myclasses.THuman;
import myclasses.THouse;
import java.util.ArrayDeque;

// класс лифта

public class TElevator {
	private int capacity; // вместимость
	private int currentFloor; // текущий этаж
	private int build_size; // общее число этажей
	private ArrayDeque<THuman> passangers; // очеретьиз пассажиров
	private ArrayDeque<Integer> floorDeque; // очереть лифта на этажи
	private int destinationFloor; // текущий этаж назначения
	private THouse building; // ссылка на здание
	
	public TElevator(THouse house,int cap)
	{
		building = house;
		capacity =  cap;
		currentFloor = 0;
		build_size = building.getFloorNum();
		passangers = new ArrayDeque<THuman>();
		floorDeque = new ArrayDeque<Integer>();
		destinationFloor = 0;
	}
	public int getPeopleNum()
	{
		return passangers.size();
	}
	public int getFloor()
	{
		return currentFloor;
	}
	public int getDestination()
	{
		return destinationFloor;
	}
	
	private int up() 
	{
		if (currentFloor>build_size) return -1;
		else currentFloor++; 
		return 0;
	} 
	private int down()
	{
		if (currentFloor<1) return -1;
		else currentFloor--;
		return 0;
	}
	
	public int in(THuman p) // запускет в лифт человека
	{
		if (isFull()) return -1;
		passangers.add(p);
		if (!floorDeque.contains(p.getDestination()));
			if (!onWay(currentFloor, destinationFloor, p.getDestination()))
				floorDeque.add(p.getDestination());
		return 0;
	}
	public boolean out() // возвращает лажь если никто не вышел
	{
		if (passangers.isEmpty()) return false;
		THuman tmp = null;
		for(THuman p : passangers)
			if (p.getDestination() == currentFloor) 
			{ tmp = p; break; }
		if (tmp!=null) { passangers.remove(tmp); return true; }
		else return false;
	}
	public boolean isSomeoneOut() // возвращает истину если на текущем этаже есть кому выйти
	{
		for(THuman p : passangers)
			if (p.getDestination() == currentFloor) return true;
		return false;
	}
	public boolean isFull()
	{
		return ( passangers.size() == capacity ? true : false );
	}
	public boolean isEmpty()
	{
		return passangers.isEmpty();
	}
	public String getString() // возвращает строку с пассажирами
	{
		String str = Integer.toString(destinationFloor) + " [";
		for(THuman p : passangers)
			str = str + " " + p.getString();
		str+=" ]";
		return str;
	}
	public void callElevator(int floor) // функция вызова лифта на указанный этаж
	{
		if (!floorDeque.contains(floor));
			if (!onWay(currentFloor, destinationFloor, floor))
				floorDeque.add(floor);
	}
	public void step() // выполняет шагэмуляции
	{	
		if (destinationFloor == currentFloor)
		{ 
			if (isSomeoneOut()) out();
			else 
				if (!building.isFloorEmpty(currentFloor) && !isFull())
					in(building.getHuman(currentFloor));
				else destinationFloor = (!floorDeque.isEmpty()) ? floorDeque.removeFirst() : 0;
		}
		else 
			if (!building.isFloorEmpty(currentFloor) && !isFull())
				in(building.getHuman(currentFloor));
			else 
				if (destinationFloor > currentFloor) up();
				else down();
	}
	// возвращает истину если этаж floor по пути от текущего до этажа назначения
	private boolean onWay(int curFloor, int dstFloor, int floor)
	{
		if (curFloor<dstFloor)
			if (curFloor<=floor && floor<=dstFloor) return true;
			else return false;
		else
			if (dstFloor<=floor && floor<=curFloor) return true;
			else return false;
	}
}
