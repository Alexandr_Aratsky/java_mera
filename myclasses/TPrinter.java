package myclasses;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.SWT;

// класс для работы с таблице на форме, отображает ("отрисовывает") состояние задния
public class TPrinter {

	private Table table; // сама таблица
	// данные библиотеки 
	private Display display; 
	private Shell sh; 
	// колонки в таблице
	private TableColumn cFloor; // колонка с номерами этажей
	private TableColumn cMine; // "шахта лифта"
	private TableColumn cTurn; // колонка отоброжающая очереди
	private TableItem[] it; // 
	private int floorNum = 0; // число этажей
	private int currentFloor; // текущей этаж
	private int lastFloor; // последний этаж
	
	public TPrinter(Display d,Table t,Shell s)
	{
		display = d; table = t; sh = s;
		cFloor = new TableColumn(table, SWT.BORDER);
	      cFloor.setText("Этаж");
	      cFloor.setAlignment(SWT.RIGHT);
		cMine = new TableColumn(table, SWT.BORDER);
	      cMine.setText("Шахта лифта");
	      cMine.setAlignment(SWT.CENTER);
	    cTurn = new TableColumn(table, SWT.NULL);
	      cTurn.setText("Очередь перед лифтом");
	      cTurn.setAlignment(SWT.LEFT);
	      
	    table.getColumn(0).pack();
	    table.getColumn(1).pack();
	    table.getColumn(2).pack();
	    
	    lastFloor = 0;
	    
	    //MessageBox mb = new MessageBox(sh);
	    //mb.setMessage("Table create");
	    //mb.open();
	}
	// "отриковка"
	public void refresh()
	{
		drawFloors();
      
	    table.getColumn(0).pack();
	    table.getColumn(1).pack();
	    table.getColumn(2).pack();
	}
	// устанавливаетчисло этажей
	public void setFloorNum(int num)
	{
		for(int i=0;i<floorNum;i++)
			it[i].dispose();
		floorNum = num;
		it = new TableItem[floorNum]; 
		for(int i=0;i<floorNum;i++)
			it[i] = new TableItem(table, SWT.NULL);
	}
	// отрисовывает номера этажей
	private void drawFloors()
	{
		cFloor.setAlignment(SWT.LEFT);
		for(int i=0;i<floorNum;i++)
		{
			it[i].setText(0, Integer.toString(i));
	    	it[i].setBackground(1,display.getSystemColor(SWT.COLOR_GRAY));
		}
	}
	// "отрисовывает" шахту лифта
	public void drawLift(int curFloor)
	{
		it[currentFloor].setBackground(1,display.getSystemColor(SWT.COLOR_GRAY));
		currentFloor = curFloor - 1;
		it[currentFloor].setBackground(1,display.getSystemColor(SWT.COLOR_WHITE));
	}
	// "отрисовывает" в шахте лифт
	public void setElev(TElevator e)
	{
		it[lastFloor].setText(1,"");
		it[e.getFloor()].setText(1,e.getString());
		lastFloor = e.getFloor();
	}
	// добавляет очереди на этажи
	public void setFloor(int flNum,String srt)
	{
		it[flNum].setText(2,srt);
	}
}
