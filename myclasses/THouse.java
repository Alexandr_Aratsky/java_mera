package myclasses;

import myclasses.THuman;
import myclasses.TElevator;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Random;

// здание с лифтом
public class THouse {
private int floorNum; // число этажей
private ArrayList<Deque<THuman>> floors; // список из очередей на этаже
private TElevator elev; // ссылка на лифт
private TPrinter pt; // ссылка на таблицу распечатывающую состояние задния
private Random rnd;
private int count; // счётчик для генерации появления нового человека
private int freq; // частота появления нового человека

public THouse(int fNum,TPrinter p,int frequency,int cap)
{
	floorNum = fNum;
	floors = new ArrayList<>(floorNum);
	for(int i=0;i<floorNum;i++)
		floors.add(new ArrayDeque<THuman>());
	elev = new TElevator(this,cap);
	pt = p; freq = frequency;
	rnd = new Random(); count = 0;
}
// шаг симуляции
public void step()
{
	// генерация случайного человка на случайном этаже
	if (count == 0) 
	{
		int f = rnd.nextInt(floorNum), d;
		do d = rnd.nextInt(floorNum); while (d == f);
		floors.get(f).add(new THuman(d)); 
		elev.callElevator(f);
	}
	
	// шаг лифта
	elev.step();
	
	// изменение таблицы
	setFlorsToTable();
	pt.setElev(elev);
	
	if (count == freq-1) count = 0;
	else count++;
}
// отправляем данные по этажам в таблицу,  что отображается на форме
private void setFlorsToTable()
{
	for(int f=0;f<floors.size();f++)
	{
		String str = "";
		for(THuman p : floors.get(f))
			str=str+" < "+p.getString();
			pt.setFloor(f, str);
	}
}
public boolean isFloorEmpty(int floor)
{
	return floors.get(floor).isEmpty();
}

public THuman getHuman(int floor)
{
	return floors.get(floor).removeFirst();
}
public int getFloorNum()
{
	return floorNum;
}

}
