package checkers;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class TScoreBoard extends JPanel
{
	private static final long	serialVersionUID	= 1L;
	private JLabel lblWhead;
	private JLabel lblBhead;
	private JLabel lblWhite;
	private JLabel lblBlack;

	public TScoreBoard(String leftTitle, String rightTitle)
	{
		super();
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gbl_panelScore = new GridBagLayout();
		gbl_panelScore.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panelScore.rowHeights = new int[]{0, 0, 0};
		gbl_panelScore.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panelScore.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gbl_panelScore);
		
		lblWhead = new JLabel(leftTitle);
		lblWhead.setForeground(new Color(0,0,0));
		lblWhead.setFont(new Font("Dialog", Font.BOLD, 15));
		GridBagConstraints gbc_lblWhead = new GridBagConstraints();
		gbc_lblWhead.insets = new Insets(0, 0, 5, 5);
		gbc_lblWhead.gridx = 0; gbc_lblWhead.gridy = 0;
		add(lblWhead, gbc_lblWhead);
		
		JLabel lblVs = new JLabel("vs");
		lblVs.setForeground(new Color(0,0,0));
		GridBagConstraints gbc_lblVs = new GridBagConstraints();
		gbc_lblVs.insets = new Insets(0, 0, 5, 5);
		gbc_lblVs.gridx = 1; gbc_lblVs.gridy = 0;
		add(lblVs, gbc_lblVs);
		
		lblBhead = new JLabel(rightTitle);
		lblBhead.setForeground(new Color(0,0,0));
		lblBhead.setFont(new Font("Dialog", Font.BOLD, 15));
		GridBagConstraints gbc_lblBhead = new GridBagConstraints();
		gbc_lblBhead.insets = new Insets(0, 0, 5, 0);
		gbc_lblBhead.gridx = 2; gbc_lblBhead.gridy = 0;
		add(lblBhead, gbc_lblBhead);
		
		lblWhite = new JLabel("0");
		lblWhite.setForeground(new Color(0,0,0));
		lblWhite.setFont(new Font("Dialog", Font.BOLD, 40));
		lblWhite.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblWhite = new GridBagConstraints();
		gbc_lblWhite.insets = new Insets(0, 0, 0, 5);
		gbc_lblWhite.gridx = 0; gbc_lblWhite.gridy = 1;
		add(lblWhite, gbc_lblWhite);
		
		JLabel label = new JLabel(":");
		label.setForeground(new Color(0,0,0));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 40));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 0, 5);
		gbc_label.gridx = 1; gbc_label.gridy = 1;
		add(label, gbc_label);
		
		lblBlack = new JLabel("0");
		lblBlack.setForeground(new Color(0,0,0));
		lblBlack.setFont(new Font("Dialog", Font.BOLD, 40));
		lblBlack.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBlack = new GridBagConstraints();
		gbc_lblBlack.gridx = 2; gbc_lblBlack.gridy = 1;
		add(lblBlack, gbc_lblBlack);
	}

	public void setTitle(String leftTitle, String rightTitle)
	{
		lblWhead.setText(leftTitle);
		lblBhead.setText(rightTitle);
	}
	public void setScore(int leftScore,int rightScore)
	{ 
		lblWhite.setText(String.valueOf(leftScore)); 
		lblBlack.setText(String.valueOf(rightScore)); 
	}

}
