package checkers;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class TSocketManager
{
	
	private InetAddress ipAddress;
	private int portNumber;
	private ServerSocket ss;
	private Socket s;
	private boolean server;
	
	public TSocketManager(int port, String ip, boolean isServer)
	{
		server = isServer;
		portNumber = port;
		if (server)
		{
			try
			{
				ss = new ServerSocket(portNumber);
				ipAddress = ss.getInetAddress();
			} catch (IOException e) { e.printStackTrace(); }
		}
		else 
		{
			try
			{
				ipAddress = InetAddress.getByName(ip);
				s = new Socket(ipAddress, portNumber);
			}
			catch (IOException e) { e.printStackTrace(); }
	   	}
	}
	
	public void startServer()
	{ 
		try { s = ss.accept(); }
		catch (IOException e) { e.printStackTrace(); } 
	}
	public void send(TDataPack pack)
	{
		
		try
		{
			ObjectOutputStream  oos = new  ObjectOutputStream(s.getOutputStream());
			oos.writeObject(pack);
			oos.close();
		}
		catch (IOException e)
		{ e.printStackTrace(); }
		
	}
	public TDataPack receive()
	{
		TDataPack res = null;
		try
		{
			ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
			try
			{ res = (TDataPack) ois.readObject(); }
			catch (ClassNotFoundException e)
			{ e.printStackTrace(); }
		}
		catch (IOException e)
		{ e.printStackTrace(); }
		return res;
	}
	
	@SuppressWarnings("static-access")
	public String getIPAddress()
	{
		try
		{ return ipAddress.getLocalHost().getHostAddress(); }
		catch (UnknownHostException e)
		{ return ""; }
	}

}
