package checkers;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;


public class TMainDialog extends JDialog
{

	private static final long	serialVersionUID	= 1L;
	private final JLabel lblPort = new JLabel("IP:");
	private final JLabel lblIP = new JLabel("Port:");
	private JTextField txtIPAdress;
	private JTextField txtPort;
	private TSelectColor panelSelect;
	private JComboBox<String> comboBox;
		
	public TMainDialog()
	{
		setResizable(false);
		setTitle("Checkers 1.0");
		setBounds(100, 100, 396, 267);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{450, 0};
		gridBagLayout.rowHeights = new int[]{237, 35, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		getContentPane().add(panel, gbc_panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(66dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));
		panel.add(lblPort, "2, 4, right, default");
		txtIPAdress = new JTextField();
		txtIPAdress.setText("127.0.0.1");
		panel.add(txtIPAdress, "4, 4, 3, 1, fill, default");
		txtIPAdress.setColumns(10);
		panel.add(lblIP, "2, 6, right, default");
		txtPort = new JTextField();
		txtPort.setText("7777");
		panel.add(txtPort, "4, 6, fill, default");
		txtPort.setColumns(10);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Classic (8x8)", "Small (6x6)", "Big (12x12)", "Wide(12x8)"}));
		panel.add(comboBox, "6, 6, fill, default");
		panelSelect = new TSelectColor();
		panelSelect.addMouseListener(new MouseAdapter() { public void mouseClicked(MouseEvent e)  { } });
		panel.add(panelSelect, "4, 8, 3, 1, fill, fill");

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		GridBagConstraints gbc_buttonPane = new GridBagConstraints();
		gbc_buttonPane.anchor = GridBagConstraints.NORTH;
		gbc_buttonPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_buttonPane.gridx = 0;
		gbc_buttonPane.gridy = 1;
		getContentPane().add(buttonPane, gbc_buttonPane);
			
		JButton okButton = new JButton("Start");
		okButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) 
			{
				Checkers.gameType = comboBox.getSelectedIndex();
				Checkers.side = panelSelect.getChoise();
				TDataPack.ip = txtIPAdress.getText();
				TDataPack.port = Integer.valueOf(txtPort.getText());
				//TSocketManager manager = new TSocketManager(TDataPack.port, TDataPack.ip, Checkers.side);
				//if (Checkers.side)
				//{
				//	TDataPack test = null;
				//	JOptionPane.showMessageDialog(null," Waiting for server...");
				//	manager.startServer();
				//	manager.send(test);
				//	manager.receive();
				//	JOptionPane.showMessageDialog(null," Connected");
				//	
				///}
				//else 
				//{
				//	TDataPack test = null;
				//	manager.receive();
				//	JOptionPane.showMessageDialog(null," Waiting for server...");
				//	manager.send(test);
				//	JOptionPane.showMessageDialog(null," Connected");
				//}
				Checkers.startGame(null);
				dispose();
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
			
		JButton cancelButton = new JButton("Close");
		cancelButton.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent arg0) { dispose(); }
			});
		buttonPane.add(cancelButton);

	}

}
