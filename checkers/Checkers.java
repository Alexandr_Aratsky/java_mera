package checkers;

import java.awt.EventQueue;

import javax.swing.JDialog;

public class Checkers
{
	public static int gameType;
	public static boolean side;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					TMainDialog dialog = new TMainDialog();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
					
				} catch (Exception e) { e.printStackTrace(); }
			}
		});
	}

	public static void startGame(TSocketManager manager)
	{
		try {
			
			TMainWindow frame = new TMainWindow(manager);
			frame.setVisible(true);
			
		} catch (Exception e) { e.printStackTrace(); }
	}
}
