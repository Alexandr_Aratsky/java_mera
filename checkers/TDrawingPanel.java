package checkers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
public class TDrawingPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private TBoard board;
	private int widthPanel; private int heightPanel;
	private int cellWidth; private int cellHeight;
	private int borderWidth; private int borderHeight;
	
	
	
	public TDrawingPanel(int width, int height, int offset)
	{
		board = new TBoard();
		widthPanel = width; 
		heightPanel = height;
		setBounds(offset, offset, width, height);
		cellWidth = (widthPanel - 30) / TCell.getBoardWidth();
		cellHeight = (heightPanel - 30) / TCell.getBoardHeight();
		borderWidth = ((widthPanel - 30) % TCell.getBoardWidth()) / 2 + 15;
		borderHeight = ((heightPanel - 30) % TCell.getBoardHeight()) / 2 + 15;
	}
	private void drawBoard(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, widthPanel, heightPanel);
		g2d.setColor(Color.BLACK);
		g2d.drawRect(0, 0, widthPanel-1, heightPanel-1);
		g2d.setColor(Color.BLACK);
		g2d.drawRect(borderWidth - 1, borderHeight - 1, widthPanel - 2*borderWidth + 1, heightPanel - 2*borderHeight + 1);
		for(int i=0;i<TCell.getBoardWidth();i++)
			for(int j=0;j<TCell.getBoardHeight();j++)
			{
				if (((i % 2 == 0) && (j % 2 == 0)) || ((i % 2 != 0) && (j % 2 != 0))) g2d.setColor(Color.LIGHT_GRAY);
				else g2d.setColor(Color.DARK_GRAY);
				g2d.fillRect(i*cellWidth + borderWidth, j*cellHeight + borderHeight,  cellWidth, cellHeight);
			}
		g2d.setColor(Color.BLACK);
		g2d.setFont(new Font("Arial", Font.BOLD, 15));
		for(int i=0;i<TCell.getBoardWidth();i++)
		{
			g2d.drawString(TCell.getXCellString(i), i*cellWidth + borderWidth + cellWidth/2, borderHeight - 3);
			g2d.drawString(TCell.getXCellString(i), i*cellWidth + borderWidth + cellWidth/2, heightPanel - 3);
		}
		for(int j=0;j<TCell.getBoardHeight();j++)
		{
			g2d.drawString(TCell.getYCellString(j), 5, j*cellHeight + borderHeight + cellHeight/2);
			g2d.drawString(TCell.getYCellString(j), widthPanel - borderHeight + 5, j*cellHeight + borderHeight + cellHeight/2);
		}
		
	}
	public void mouseClick(int X,int Y)
	{
		board.cellClick(new TCell((X - borderWidth) / cellWidth, (Y - borderHeight) / cellHeight));
		repaint();
	}
	private void drawStates(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
		
		for(int i=0;i<TCell.getBoardWidth();i++)
			for(int j=0;j<TCell.getBoardHeight();j++)
			{
				TState tmp = board.getState(new TCell(i, j));
				if (tmp == TState.SELECTED) g2d.setColor(Color.GREEN);
				else if (tmp == TState.CAN_MOVE) g2d.setColor(Color.YELLOW);
				else if (tmp == TState.CAN_KILL) g2d.setColor(Color.RED);
				else continue;
				g2d.drawRect(i*cellWidth + borderWidth + 1, j*cellHeight + borderHeight + 1, 
							cellWidth - 1, cellHeight - 1);
			}
	}
	private void drawCheckers(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setFont(new Font("Arial", Font.BOLD, 25));
		
		for(int i=0;i<TCell.getBoardWidth();i++)
			for(int j=0;j<TCell.getBoardHeight();j++)
			{
				TFigure tmp = board.getFugure(new TCell(i, j));
				if (tmp == TFigure.EMPTY) continue;
				if (tmp == TFigure.WHITE || tmp == TFigure.WHITE_QWEEN) g2d.setColor(Color.WHITE);
				else if (tmp == TFigure.BLACK || tmp == TFigure.BLACK_QWEEN) g2d.setColor(Color.BLACK);
				else continue;
				g2d.fillOval(i*cellWidth + borderWidth + (int) (cellWidth*0.2), j*cellHeight + borderHeight + (int) (cellHeight*0.2), 
						(int) (cellWidth*0.6), (int) (cellHeight*0.6));
				if (!TFigure.isQween(tmp)) continue;
				else if (tmp == TFigure.WHITE_QWEEN) g2d.setColor(Color.BLACK);
				else g2d.setColor(Color.WHITE);
				g2d.drawString("K", i*cellWidth + borderWidth + (int) (cellWidth*0.6), j*cellHeight + borderHeight + (int) (cellHeight*0.4));
				
			}
	}
	
	public void paint(Graphics g) 
	{
		drawBoard(g);
		drawCheckers(g);
		drawStates(g);
		
	}

	public void skip()
	{
		board.skip();
		repaint();
	}
	public boolean step()
	{
		boolean res = board.step();
		repaint();
		return res;
	}
	
	public int getScoreW()
	{
		return board.getScoreWhite();
	}
	public int getScoreB()
	{
		return board.getScoreBlack();
	}
	public String getStepString()
	{
		return board.getStepString();
	}
	
}
