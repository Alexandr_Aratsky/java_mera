package checkers;

public enum TState 
{
	SELECTED, CAN_MOVE, CAN_KILL, EMPTY;
	
	public static boolean isEmpty(TState state)
	{
		if (state == TState.EMPTY) return true;
		return false;
	}
	public static boolean isCanMove(TState state)
	{
		if (state == TState.CAN_MOVE) return true;
		return false;
	}
	public static boolean isCanKill(TState state)
	{
		if (state == TState.CAN_KILL) return true;
		return false;
	}
	

}
