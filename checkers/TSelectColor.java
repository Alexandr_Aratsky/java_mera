package checkers;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class TSelectColor extends JPanel
{
	private static final long	serialVersionUID	= 1L;

	private boolean isServer = true;
	
	private JPanel panelLeft;
	private JPanel panelRight;
	
	public TSelectColor()
	{
		super();
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		setLayout( new FormLayout( new ColumnSpec[] {
				ColumnSpec.decode("default:grow"),
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				RowSpec.decode("max(80dlu;default):grow"),}));
		
		panelLeft = new JPanel();
		panelLeft.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) 
				{
					panelRight.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
					panelLeft.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
					repaint(); isServer = !isServer;
				}
				public void mouseEntered(MouseEvent e) 
				{ setCursor(new Cursor(Cursor.HAND_CURSOR)); }
				public void mouseExited(MouseEvent e) 
				{ setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); }
			});
		panelLeft.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		add(panelLeft, "1, 1, fill, fill");

		panelRight = new JPanel();
		panelRight.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) 
				{
					panelLeft.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
					panelRight.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
					repaint(); isServer = !isServer;
				}
				public void mouseEntered(MouseEvent e) 
				{ setCursor(new Cursor(Cursor.HAND_CURSOR)); }
				public void mouseExited(MouseEvent e) 
				{ setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); }
			});
		panelRight.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(panelRight, "2, 1, fill, fill");

	}

	public void paint(Graphics g) 
	{
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setFont(new Font("Arial", Font.BOLD, 30));
		int size = (panelLeft.getWidth() < panelLeft.getHeight() ? panelLeft.getWidth() : panelLeft.getHeight()) * 9/10;
		g2d.setColor(Color.WHITE);
		g2d.fillOval(panelLeft.getWidth()/2 - size/2, panelLeft.getHeight()/2 - size/2, size, size);
		g2d.setColor(Color.BLACK);
		g2d.fillOval(panelRight.getWidth()/2 - size/2 + panelLeft.getWidth(), panelRight.getHeight()/2 - size/2, size, size);
		g2d.drawString("SERVER", panelLeft.getWidth()/2 - 60, panelLeft.getHeight()/2 + 15);
		g2d.setColor(Color.WHITE);
		g2d.drawString("CLIENT", panelRight.getWidth()/2 - 55 + panelLeft.getWidth(), panelRight.getHeight()/2 + 15);
		
	}

	public boolean getChoise()
	{
		return isServer;
	}
}
