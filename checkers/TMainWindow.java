package checkers;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.GridBagLayout;

import javax.swing.JEditorPane;

import java.awt.GridBagConstraints;

import javax.swing.border.LineBorder;

import java.awt.Color;


public class TMainWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private TDrawingPanel panelBoard;
	private TScoreBoard score;
	private TJornal jornal;
	private TSocketManager sManager;
	private int width = 400;
	private int height = 400;
	private final int infoSize = 165;
	
	
	private void setStepSettings()
	{
		score.setScore(panelBoard.getScoreB(), panelBoard.getScoreW()); 
		jornal.setTitle("step -> " + TFigure.getCurrentColorString());
		jornal.appendString("\n " + (TFigure.getCurrentColorBoolean() ? "B: " : "W: ") + panelBoard.getStepString());
	}
	
	private void setBoardSize()
	{
		switch (Checkers.gameType)
		{
			case 0:
				width = 530;
				height = 530;
				TCell.setBoardBounds(8, 8);
				break;
			case 1:
				width = 480;
				height = 480;
				TCell.setBoardBounds(6, 6);
				break;
			case 2:
				width = 600;
				height = 600;
				TCell.setBoardBounds(12, 12);
				break;
			case 3:
				width = 630;
				height = 430;
				TCell.setBoardBounds(12, 8);
				break;
			default:
				break;
		}
	}
		
	public TMainWindow(TSocketManager manager) {
		
		setBoardSize();
		sManager = manager;
		
		setTitle("Checkers 1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		
		panelBoard = new TDrawingPanel(width,height,30);
		panelBoard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) { panelBoard.mouseClick(e.getX(), e.getY()); } });
		score = new TScoreBoard("WHITE","BLACK");
		jornal = new TJornal(" Jornal of moves");
		
		JButton btnStep = new JButton("Step");
		btnStep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{ if (!panelBoard.step()) JOptionPane.showMessageDialog(null, "You did not move any checker!");
				else setStepSettings(); } });
		
		JButton btnSkip = new JButton("Skip");
		btnSkip.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{ 
				panelBoard.step(); setStepSettings(); 
				
			}
		});
				
		JToggleButton btnSend = new JToggleButton("Send");
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JEditorPane editorPane = new JEditorPane();
		GridBagConstraints gbc_editorPane = new GridBagConstraints();
		gbc_editorPane.fill = GridBagConstraints.BOTH;
		gbc_editorPane.gridx = 0;
		gbc_editorPane.gridy = 0;
		panel.add(editorPane, gbc_editorPane);
		
		int h = height - 80;
		
		setBounds(30, 30, width + infoSize + 70, height + 80);
		score.setBounds(width + 45, 30, infoSize, h * 2/10);
		jornal.setBounds(width + 45, h * 2/10 + 35, infoSize, h * 5/10);
		btnStep.setBounds(width + 45, h * 7/10 + 40, infoSize * 5/10, h * 1/20);
		btnSkip.setBounds(width + infoSize * 5/10 + 45, h * 7/10 + 40, infoSize * 4/10, h * 1/20);
		btnSend.setBounds(width + 45, h * 19/20 + 50, infoSize * 3/5, h * 1/20);
		panel.setBounds(width + 45, h * 15/20 + 45, infoSize, h * 2/10);
		
		contentPane.add(panel);
		contentPane.add(btnSend);
		contentPane.add(btnSkip);
		contentPane.add(panelBoard);
		contentPane.add(score);
		contentPane.add(jornal);
		contentPane.add(btnStep);		
		
		setStepSettings();
		jornal.clearArea();
	}
}
