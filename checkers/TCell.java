package checkers;


enum TMoveVector { UP_RIGHT, DOWN_RIGHT, UP_LEFT, DOWN_LEFT };

// класс хранящий координаты клеток доски
public class TCell
{
	public int X;
	public int Y;
	
	// длина и ширина доски (общие для всех объектов)
	public static final int defaultBoardSize = 8;
	private static int boardWidth = defaultBoardSize;
	private static int boardHeight = defaultBoardSize;
	
	public static int getBoardWidth()
	{
		return boardWidth;
	}
	public static int getBoardHeight()
	{
		return boardHeight;
	}
	public static void setBoardBounds(int W, int H)
	{
		if (W<0 || H<0) 
		{ boardWidth = defaultBoardSize; boardHeight = defaultBoardSize; }
		else { boardWidth = W; boardHeight = H; }
	}
		
	public TCell()
	{
		X = 0; Y = 0;
	}
	public TCell(int x, int y)
	{
		X = x; Y = y;
	}
	public TCell(TCell cell)
	{
		X = cell.X; Y = cell.Y;
	}
	
	public static boolean inBoardBounds(int x, int y)
	{
		if (x > boardWidth - 1 || x < 0) return false;
		else if (y > boardHeight - 1 || y < 0) return false;
		return true;
	}
	public static boolean inBoardBounds(TCell cell)
	{
		return inBoardBounds(cell.X, cell.Y);
	}
	public static int cellsDistance(TCell c1,TCell c2)
	{
		if (Math.abs(c1.X-c2.X) != Math.abs(c1.Y-c2.Y)) return -1;
		else return Math.abs(c1.X-c2.X);
	}
	
	public boolean simpleMove(TMoveVector vector)
	{
		int x_V = 0 , y_V = 0;
		switch (vector) {
		case UP_RIGHT: x_V = +1; y_V = -1; break;
		case UP_LEFT: x_V = -1; y_V = -1; break;
		case DOWN_LEFT: x_V = -1; y_V = +1; break;
		case DOWN_RIGHT: x_V = +1; y_V = +1; break;
		default: break; }
		if (inBoardBounds(X+x_V, Y+y_V)) { X+=x_V; Y+=y_V; return true; }
		else return false;
	}
	public boolean add(TCell vector)
	{
		if (inBoardBounds(X+vector.X, Y+vector.Y)) { X+=vector.X; Y+=vector.Y; return true; }
		else return false;
	}
	public boolean equels(TCell cell)
	{
		if (cell.X == X && cell.Y == Y) return true;
		else return false;
	}
	
	public static boolean hasCellOnLine(TCell cell,TCell start,TCell finish)
	{
		boolean x,y;
		if ((start.X>cell.X && finish.X<cell.X) || (start.X<cell.X && finish.X>cell.X)) x = true;
		else x = false;
		if ((start.Y>cell.Y && finish.Y<cell.Y) || (start.Y<cell.Y && finish.Y>cell.Y)) y = true;
		else y = false;
		if (x && y) return true;
		else return false;
	}

	public static String getCelllString(TCell cell)
	{
		return "(" + TCell.getXCellString(cell.X) + ":" + TCell.getYCellString(cell.Y) + ")";
	}
	public static String getXCellString(int x)
	{ return String.valueOf(Character.toChars(65 + x)); }
	public static String getYCellString(int y)
	{ return String.valueOf(y + 1); }
}
