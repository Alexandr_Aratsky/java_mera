package emulator;


import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.graphics.Point;

// импорт моих классов
import myclasses.THouse;
import myclasses.TPrinter;





public class TWindow {

	/* элементы SWT библиотеки */
	protected Shell shell; 
	private Display display;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	// число этажей 
	private Text txtFloorNum;
	// число вместимости лифта
	private Text txtEmpLift;
	// частота появления пассажиров
	private Text txtFreqNum;
	// число тактов эмуляции
	private Text txtNumEmu;
	// таблица представляющая собой здание
	private Table table; 
	// класс для формирования таблицы
	private TPrinter pt;
	// класс для эмуляции здания
	private THouse b;
	// дополнительные переменные необходимые для работы интерфейса
	private boolean byStep = false;
	private boolean done;
	private int counter = 0;
	private Label labelcounter;

	public static void main(String[] args) {
		try {
			TWindow TWindow = new TWindow();
			TWindow.open();
		} catch (Exception e) { e.printStackTrace(); }
	}
	// открытие GUI окна
	public void open() {
		display = Display.getDefault();
		// создание кнопок и прочего вынес в отдельный приватный метод
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	// создаёт форму, расставляет элементы  
	protected void createContents() 
	{
		shell = new Shell();
		shell.setMinimumSize(new Point(720, 600));
		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		shell.setSize(720, 600);
		shell.setText("Эмулятор лифта");
		create_interface();
		table_create();
		
		final Button btnStart = new Button(shell, SWT.NONE);
		btnStart.setEnabled(false);
		btnStart.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) { start_btn_click(); }
		});
		btnStart.setBounds(578, 234, 91, 29);
		formToolkit.adapt(btnStart, true, true);
		btnStart.setText("Начать");
		
		final Button button = new Button(shell, SWT.NONE);
		button.setEnabled(false);
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) { next_bt_click(); }
		});
		button.setText("След. шаг");
		button.setBounds(578, 299, 91, 29);
		formToolkit.adapt(button, true, true);
		
		Button btnCheckButton = new Button(shell, SWT.CHECK);
		btnCheckButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) { byStep = !byStep; button.setEnabled(byStep); }
		});
		btnCheckButton.setBounds(554, 269, 115, 24);
		formToolkit.adapt(btnCheckButton, true, true);
		btnCheckButton.setText("Пошаговый");
		
		Button btnNewButton = new Button(shell, SWT.NONE);
		// 
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					pt.setFloorNum(Integer.parseInt(txtFloorNum.getText()));
				} catch (Exception ex) { ex.printStackTrace(); }	
				btnStart.setEnabled(true);
			   pt.refresh();
			}
		});
		btnNewButton.setText("OK");
		btnNewButton.setBounds(536, 234, 36, 29);
		formToolkit.adapt(btnNewButton, true, true);
		
		labelcounter = new Label(shell, SWT.NONE);
		labelcounter.setFont(SWTResourceManager.getFont("Ubuntu", 12, SWT.NORMAL));
		labelcounter.setBounds(536, 299, 36, 29);
		formToolkit.adapt(labelcounter, true, true);
		labelcounter.setText("---");

	}
	
	// запускает симуляцию в отдельном потоке, останавливется при смене флага byStep
	void start_btn_click()
	{ 
		
		for(int i=counter;i<Integer.parseInt(txtNumEmu.getText());i++) {
			if (byStep) { done = true; display.wake(); return; }
			done = false;
			new Thread() { 
				public void run() {
					try {Thread.sleep(500);} catch (Throwable th) { }
					done = true;
					display.wake();  }
			}	.start();
		    while (!done) {
		        if (!display.readAndDispatch()) display.sleep();
		    }
			b.step();
			labelcounter.setText(String.valueOf(counter++));
			pt.refresh();
		}

	}
	void next_bt_click(){
		b.step();
		labelcounter.setText(String.valueOf(counter++));
		pt.refresh();
	}
	// формирует таблицу
	void table_create(){
		
		Group group = new Group(shell, SWT.NONE);
		group.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		group.setBounds(10, 10, 520, 500);
		
		table = formToolkit.createTable(group, SWT.BORDER);
		table.setLinesVisible(true);
		table.setSize(520, 500);
		table.setHeaderVisible(true);

	    pt = new TPrinter(display, table, shell); 
	    b = new THouse(Integer.parseInt(txtFloorNum.getText()), pt, Integer.parseInt(txtFreqNum.getText()), Integer.parseInt(txtEmpLift.getText()));
	    
		table.setBounds(0, 0, 520, 500);
		formToolkit.paintBordersFor(table);
		table.setHeaderVisible(true);
		
		
	}
	// формирует текст и кнопки на форме
	void create_interface(){
		
		txtFloorNum = formToolkit.createText(shell, "20", SWT.BORDER);
		txtFloorNum.setBounds(536, 33, 133, 27);
		
		Label label = formToolkit.createLabel(shell, "Число этажей", SWT.NONE);
		label.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		label.setBounds(536, 10, 148, 17);
		
		Label lblAsdfasfa = new Label(shell, SWT.NONE);
		lblAsdfasfa.setBounds(536, 66, 148, 17);
		formToolkit.adapt(lblAsdfasfa, true, true);
		lblAsdfasfa.setText("Вместимость лифта");
		
		txtEmpLift = new Text(shell, SWT.BORDER);
		txtEmpLift.setText("4");
		txtEmpLift.setBounds(536, 89, 133, 27);
		formToolkit.adapt(txtEmpLift, true, true);
		
		Label lblXasf = new Label(shell, SWT.NONE);
		lblXasf.setBounds(536, 122, 148, 17);
		formToolkit.adapt(lblXasf, true, true);
		lblXasf.setText("Частота появления");
		
		txtFreqNum = new Text(shell, SWT.BORDER);
		txtFreqNum.setText("6");
		txtFreqNum.setBounds(536, 145, 133, 27);
		formToolkit.adapt(txtFreqNum, true, true);
		
		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setBounds(536, 178, 148, 17);
		formToolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText("Число тактов теста");
		
		txtNumEmu = new Text(shell, SWT.BORDER);
		txtNumEmu.setText("2000");
		txtNumEmu.setBounds(536, 201, 133, 27);
		formToolkit.adapt(txtNumEmu, true, true);
	}
}
