package threads_example;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLocker extends ReentrantReadWriteLock
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private CyclicBarrier barrier = new CyclicBarrier(2);
	private int count = 0;
	
	public ReadWriteLocker()
	{
		super(true);
	}
	
	public void waitParent() throws Exception
	{
		if (barrier.getNumberWaiting() == 0) barrier.await();
	}
	public void parentArraved()
	{
		if (count==0) return;
		try
		{ barrier.await(); }
		catch (InterruptedException | BrokenBarrierException e)
		{ e.printStackTrace(); }
	}
	public String getQueuedReaderString()
	{
		String queuedReader = "";
		for(Thread t : super.getQueuedReaderThreads()) queuedReader = queuedReader + "->" + t.getName();
		return queuedReader;
	}
	public String getQueuedWriterString()
	{
		String queuedWriter = "";
		for(Thread t : super.getQueuedWriterThreads()) queuedWriter = queuedWriter + "->" + t.getName();
		return queuedWriter;
	}
	public String toString()
	{
		return super.toString();
	}
	public void start(int maxThreads)
	{
		count = maxThreads;
	}
	public synchronized void done()
	{
		count--;
	}
}
