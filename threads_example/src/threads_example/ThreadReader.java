package threads_example;

public class ThreadReader extends ThreadWorker
{


	public ThreadReader(String id,MainForm MF, int WN)
	{
		super(MF, WN);
		setName(id);
	}

	@Override
	protected void doWork()
	{
		mf.rwLock.readLock().lock();
		try
		{ mf.addNodeToPane(false, getName(), "I'm reading."); mf.rwLock.waitParent(); }
		catch(Exception e) { e.printStackTrace();}
		finally
		{ mf.rwLock.readLock().unlock(); }
	}

	@Override
	protected void finishedWork()
	{
		mf.addNodeToPane(false, getName(), "I have finished.");
	}

}
