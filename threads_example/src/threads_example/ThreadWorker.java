package threads_example;

public abstract class ThreadWorker extends  Thread
{
	protected MainForm mf;
	private int writingNum;
	
	
	public ThreadWorker(MainForm MF, int WN)
	{
		mf = MF;
		writingNum = WN;
	}
	
	@Override
	public void run()
	{
		for(int i = 0; i<writingNum; i++)
		{
			try { Thread.sleep((int)(Math.random()%25)); } catch (InterruptedException e) { e.printStackTrace(); }
			doWork();
		}
		finishedWork();
		mf.rwLock.done();
	}

	protected abstract void doWork();
	protected abstract void finishedWork();
}
