package threads_example;

public class ThreadWriter extends ThreadWorker
{
	public ThreadWriter(String id,MainForm MF, int WN)
	{
		super(MF, WN);
		setName(id);
	}

	@Override
	protected void doWork()
	{
		mf.rwLock.writeLock().lock();
		try
		{ mf.addNodeToPane(true, getName(), "I'm writing."); mf.rwLock.waitParent(); }
		catch(Exception e) { e.printStackTrace();}
		finally
		{ mf.rwLock.writeLock().unlock(); }
	}


	@Override
	protected void finishedWork()
	{
		mf.addNodeToPane(true, getName(), "I have finished.");
	}
	
}
