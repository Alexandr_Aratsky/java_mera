package threads_example;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.AbstractButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainForm extends JFrame
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private JPanel	contentPane;
	private JTable tableMemory;
	private JTextField txtWritersnum;
	private JTextField txtReadersnum;
	private JTextPane textPane;
	private int count = 0;
	private boolean threadsStarted = false;
	private JSpinner spinner;
	//public ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock(true);
	public ReadWriteLocker rwLock = new ReadWriteLocker();
	private JButton	btnStart;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MainForm frame = new MainForm();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Append coloring text to TextPane
	 */
	private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
	public synchronized void addNodeToPane(boolean isWriter, String idThread, String msgThread)
	{
		appendToPane(textPane, ++count + " :: ", Color.RED);
		if (isWriter) appendToPane(textPane, "writer(" + idThread + ")", Color.BLUE);
		else appendToPane(textPane, "reader(" + idThread + ")", new Color(0,127,0));
		appendToPane(textPane, " -> ", Color.GRAY);
		appendToPane(textPane, "\"" + msgThread + "\"" + "\n", Color.BLACK);
	}
	
	
	/**
	 * Starting threads work
	 */
	private void startThreads()
	{
		int wNum, rNum;
		try 
		{
			rNum = Integer.parseInt(txtReadersnum.getText());
			wNum = Integer.parseInt(txtWritersnum.getText());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Please, input number of writers and readers!");
			return;
		}
		rwLock.start(rNum+wNum);
		for(int i = 1; i <= rNum; i++)
			new ThreadReader(String.valueOf(i),this,(int) spinner.getValue()).start();
		for(int i = 1; i <= wNum; i++)
			new ThreadWriter(String.valueOf(i),this,(int) spinner.getValue()).start();
		
		threadsStarted = true; 
		btnStart.setText("Step");
	}
	public void showState()
	{
		tableMemory.setValueAt(rwLock.getQueuedReaderString(), 0, 1);
		tableMemory.setValueAt(rwLock.getQueuedWriterString(), 1, 1);
		tableMemory.setValueAt(rwLock.toString(), 2, 1);
		tableMemory.setValueAt(rwLock.readLock().toString(), 4, 1);
		tableMemory.setValueAt(rwLock.writeLock().toString(), 3, 1);
	}
	
	/**
	 * Create the frame.
	 */
	public MainForm()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 743, 218);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 223, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		txtWritersnum = new JTextField();
		txtWritersnum.setToolTipText("Число писателей");
		txtWritersnum.setText("WritersNum");
		GridBagConstraints gbc_txtWritersnum = new GridBagConstraints();
		gbc_txtWritersnum.insets = new Insets(0, 0, 5, 5);
		gbc_txtWritersnum.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtWritersnum.gridx = 0;
		gbc_txtWritersnum.gridy = 0;
		contentPane.add(txtWritersnum, gbc_txtWritersnum);
		txtWritersnum.setColumns(10);
		
		txtReadersnum = new JTextField();
		txtReadersnum.setToolTipText("Число читателей");
		txtReadersnum.setText("ReadersNum");
		GridBagConstraints gbc_txtReadersnum = new GridBagConstraints();
		gbc_txtReadersnum.insets = new Insets(0, 0, 5, 5);
		gbc_txtReadersnum.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtReadersnum.gridx = 1;
		gbc_txtReadersnum.gridy = 0;
		contentPane.add(txtReadersnum, gbc_txtReadersnum);
		txtReadersnum.setColumns(10);
		
		spinner = new JSpinner();
		spinner.setToolTipText("Число обращений к данным");
		spinner.setModel(new SpinnerNumberModel(10, 1, 100, 1));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 2;
		gbc_spinner.gridy = 0;
		contentPane.add(spinner, gbc_spinner);
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if (threadsStarted) { rwLock.parentArraved(); showState(); }
				else { startThreads(); showState(); } 
			}
		});
		GridBagConstraints gbc_btnStart = new GridBagConstraints();
		gbc_btnStart.insets = new Insets(0, 0, 5, 5);
		gbc_btnStart.gridx = 3;
		gbc_btnStart.gridy = 0;
		contentPane.add(btnStart, gbc_btnStart);
		
		JLabel lblMemorystate = new JLabel("MemoryState");
		GridBagConstraints gbc_lblMemorystate = new GridBagConstraints();
		gbc_lblMemorystate.insets = new Insets(0, 0, 5, 0);
		gbc_lblMemorystate.gridx = 4;
		gbc_lblMemorystate.gridy = 0;
		contentPane.add(lblMemorystate, gbc_lblMemorystate);
		
		tableMemory = new JTable();
		tableMemory.setBorder(new LineBorder(new Color(0, 0, 0)));
		tableMemory.setModel(new DefaultTableModel(
			new Object[][] {
				{"QueuedReader", null},
				{"QueuedWriter", null},
				{"ReadWriteLock", null},
				{"WriteLock", null},
				{"ReadLock", null},
			},
			new String[] {
				"Key", "Value"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tableMemory.getColumnModel().getColumn(0).setResizable(false);
		tableMemory.getColumnModel().getColumn(0).setPreferredWidth(105);
		tableMemory.getColumnModel().getColumn(0).setMaxWidth(105);
		tableMemory.getColumnModel().getColumn(1).setResizable(false);
		
		textPane = new JTextPane();
		textPane.setText("");
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 4;
		gbc_textArea.insets = new Insets(0, 0, 0, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = 1;
		JScrollPane scroll = new JScrollPane (textPane,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scroll,gbc_textArea);
		GridBagConstraints gbc_tableMemory = new GridBagConstraints();
		gbc_tableMemory.fill = GridBagConstraints.BOTH;
		gbc_tableMemory.gridx = 4;
		gbc_tableMemory.gridy = 1;
		contentPane.add(tableMemory, gbc_tableMemory);
	}

	
	
	
}
