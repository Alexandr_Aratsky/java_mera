package library;

import java.util.Random;

// поток реализующий алгоритм писателя
// пишет случайное число раз с со случайными перерывами по времени
public class TWriter implements Runnable {

	private String id;
	private TLibrary rw;
	
	public TWriter(String id_, TLibrary rw_)
	{
		id = id_;
		rw = rw_;
	}
	public void run() {
		Random rnd = new Random();
		do {
			write();
			try { Thread.sleep(rnd.nextInt(1000)+ 500); } catch (InterruptedException e) { e.printStackTrace(); }
		} while (rnd.nextInt(3) != 0);
		rw.append(id + " -> done\n");	
	}
	public void write()
	{
		while (!rw.getWmutex(id)) ;
		while (!rw.getRmutex(id)) ;
		rw.freeWmutex();
		rw.append(id + " -> writing ...\n");
		try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
		rw.freeRmutex();
	}

}
