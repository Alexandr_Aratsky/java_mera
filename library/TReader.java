package library;

import java.util.Random;
// поток реализующий алгоритм читателя
// читает случайное число раз с со случайными перерывами по времени
public class TReader implements Runnable {

	private String id;
	private TLibrary rw;
	
	public TReader(String id_, TLibrary rw_)
	{
		id = id_;
		rw = rw_;
	}
	public void run() {
		Random rnd = new Random();
		do {
			read();
			try { Thread.sleep(rnd.nextInt(1000)+ 500); } catch (InterruptedException e) { e.printStackTrace(); }
		} while (rnd.nextInt(3) != 0);
		rw.append(id + " -> done\n");	
	}
	public void read()
	{
		while (!rw.getWmutex(id)) ;
		while (!rw.getRCmutex(id)) ;
		if (rw.rcount == 0) while (!rw.getRmutex(id)) ;
		rw.rcount++; rw.printRCount();
		rw.freeRCmutex();
		rw.freeWmutex();
		rw.append(id + " -> reading ...\n");
		try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
		while (!rw.getRCmutex(id)) ;
		rw.rcount--; rw.printRCount();
		if (rw.rcount == 0) rw.freeRmutex();
		rw.freeRCmutex();
	}

}

