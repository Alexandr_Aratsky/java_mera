package library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTextArea;

// главное окно
public class TLibrary extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtWriters;
	private JTextField txtReaders;
	// аналог мьютексов для решения задачи
	private boolean w_mutex; 
	private boolean r_mutex;
	private boolean rc_mutex;
	// счётчик читателей
	public int rcount;
	// для вывода информации по мьютексам
	private JLabel lblNewLabel;
	private JTextField textField;
	private JLabel lblNewLabel_1;
	private JTextField textField_1;
	private JLabel lblNewLabel_2;
	private JTextField textField_2;
	private JLabel lblNewLabel_3;
	private JTextField textField_3;
	// сюда выводятся состояния потоков
	private JTextArea textArea;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TLibrary frame = new TLibrary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// добавляет строку в конец textArea
	public void append(String s) 
	{
		   try {
		      Document doc = textArea.getDocument();
		      doc.insertString(doc.getLength(), s, null);
		   } catch(BadLocationException exc) {
		      exc.printStackTrace();
		   }
	}
	
	// функции доступа к мьютексам, id - строка идентификатор потока
	public boolean getWmutex(String id)
	{
		if (w_mutex) { w_mutex = false; textField.setText(id); return true; }
		else return false;
	}
	public void freeWmutex()
	{
		w_mutex = true;
		textField.setText("---");
	}
	public boolean getRmutex(String id)
	{
		if (r_mutex) { r_mutex = false; textField_1.setText(id); return true; }
		else return false;
	}
	public void freeRmutex()
	{
		r_mutex = true;
		textField_1.setText("---");
	}
	public boolean getRCmutex(String id)
	{
		if (rc_mutex) { rc_mutex = false; textField_2.setText(id); return true; }
		else return false;
	}
	public void freeRCmutex()
	{
		rc_mutex = true;
		textField_2.setText("---");
	}
	public void printRCount()
	{
		textField_3.setText(String.valueOf(rcount));
	}
	
	// запуск эмуляции
	public void start()
	{
		for(int i=1;i<=Integer.parseInt(txtWriters.getText());i++)
		{
			Thread writerThread = new Thread(new TWriter("writer"+ i, this));
			writerThread.start();
		}
		for(int i=1;i<=Integer.parseInt(txtReaders.getText());i++)
		{
			Thread readerThread = new Thread(new TReader("reader"+ i, this));
			readerThread.start();
		}
	}
	
	public TLibrary() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 431, 380);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 127, 114, 120, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 25, 18, 19, 15, 19, 15, 19, 15, 124, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		txtWriters = new JTextField();
		txtWriters.setText("Writers");
		GridBagConstraints gbc_txtWriters = new GridBagConstraints();
		gbc_txtWriters.anchor = GridBagConstraints.WEST;
		gbc_txtWriters.insets = new Insets(0, 0, 5, 5);
		gbc_txtWriters.gridx = 1;
		gbc_txtWriters.gridy = 1;
		getContentPane().add(txtWriters, gbc_txtWriters);
		txtWriters.setColumns(10);
		
		txtReaders = new JTextField();
		txtReaders.setText("Readers");
		GridBagConstraints gbc_txtReaders = new GridBagConstraints();
		gbc_txtReaders.anchor = GridBagConstraints.WEST;
		gbc_txtReaders.insets = new Insets(0, 0, 5, 5);
		gbc_txtReaders.gridx = 2;
		gbc_txtReaders.gridy = 1;
		getContentPane().add(txtReaders, gbc_txtReaders);
		txtReaders.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Start");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start();
				
			}
		});
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.anchor = GridBagConstraints.NORTH;
		gbc_btnNewButton_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 3;
		gbc_btnNewButton_1.gridy = 1;
		getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
		
		lblNewLabel = new JLabel("w_mutex");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 3;
		gbc_lblNewLabel.gridy = 2;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		textField.setText("---");
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 3;
		getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);
		
		lblNewLabel_1 = new JLabel("r_mutex");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 3;
		gbc_lblNewLabel_1.gridy = 4;
		getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setText("---");
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 3;
		gbc_textField_1.gridy = 5;
		getContentPane().add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		lblNewLabel_2 = new JLabel("rc_mutex");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 3;
		gbc_lblNewLabel_2.gridy = 6;
		getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		textField_2 = new JTextField();
		textField_2.setText("---");
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.anchor = GridBagConstraints.WEST;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.gridx = 3;
		gbc_textField_2.gridy = 7;
		getContentPane().add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		lblNewLabel_3 = new JLabel("rcount");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 8;
		getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridheight = 8;
		gbc_textArea.gridwidth = 2;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 2;
		JScrollPane scroll = new JScrollPane (textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scroll,gbc_textArea);
		
		textField_3 = new JTextField();
		textField_3.setText("0");
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.anchor = GridBagConstraints.NORTHWEST;
		gbc_textField_3.gridx = 3;
		gbc_textField_3.gridy = 9;
		getContentPane().add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);
		
		w_mutex = true;
		r_mutex = true;
		rc_mutex = true;
		rcount = 0;

	}
}
