package checkers_online;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class TMainWindow extends JFrame implements IStepable
{

	private static final long		serialVersionUID	= 1L;
	private JPanel					contentPane;
	private TDrawingPanel			panelBoard;
	private TScoreBoard				score;
	private TJornal					jornal;
	private int						width;
	private int						height;
	private final int				infoSize			= 180;
	private final TSocketManager	manager;
	private final boolean			mySide;

	private void setStepSettings()
	{
		score.setScore(panelBoard.getScoreB(), panelBoard.getScoreW());
		jornal.setTitle("step -> " + TFigure.getCurrentColorString());
		jornal.appendString("\n "
				+ (TFigure.getCurrentColorBoolean() ? "B: " : "W: ")
				+ panelBoard.getStepString());
	}

	private void waitStep()
	{
		setCursor(Cursor.WAIT_CURSOR);
		(new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				panelBoard.step(manager.receive());
				setStepSettings();
			}
		})).start();
		setCursor(Cursor.DEFAULT_CURSOR);
	}

	public TMainWindow(Pair<Integer, Integer> board,
			Pair<Integer, Integer> pixels, boolean side,
			final TSocketManager manager)
	{
		width = pixels.getFirst();
		height = pixels.getSecond();
		TCell.setBoardBounds(board.getFirst(), board.getSecond());
		TBoard.setOwn(this);

		this.manager = manager;
		this.mySide = side;

		setTitle("Checkers 1.0 [" + ((mySide) ? " WHITE ]" : " BLACK ]"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelBoard = new TDrawingPanel(width, height, 30);
		panelBoard.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)
			{
				if (mySide == TFigure.getCurrentColorBoolean())
					panelBoard.mouseClick(e.getX(), e.getY());
			}
		});
		score = new TScoreBoard("WHITE", "BLACK");
		jornal = new TJornal(" Jornal of moves");

		JButton btnStep = new JButton("Step");
		btnStep.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if (mySide != TFigure.getCurrentColorBoolean())
				{
					JOptionPane.showMessageDialog(null, "It's not your turn!");
					return;
				}
				else if (!panelBoard.step(null))
					JOptionPane.showMessageDialog(null,
							"You did not move any checker!");
				else
					setStepSettings();
			}
		});

		JButton btnGiveUp = new JButton("Give Up");
		btnGiveUp.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (mySide != TFigure.getCurrentColorBoolean())
				{
					JOptionPane.showMessageDialog(null, "It's not your turn!");
					return;
				}
				panelBoard.giveUp();
			}
		});

		int h = height - 80;

		setBounds(30, 30, width + infoSize + 70, height + 80);
		score.setBounds(width + 45, 30, infoSize, h * 2 / 10);
		jornal.setBounds(width + 45, h * 2 / 10 + 35, infoSize, h * 5 / 10);
		btnStep.setBounds(width + 45, h * 7 / 10 + 40, infoSize * 5 / 10,
				h * 1 / 20);
		btnGiveUp.setBounds(width + infoSize * 6 / 10 + 45, h * 7 / 10 + 40,
				infoSize * 4 / 10, h * 1 / 20);
		contentPane.add(btnGiveUp);
		contentPane.add(panelBoard);
		contentPane.add(score);
		contentPane.add(jornal);
		contentPane.add(btnStep);

		setStepSettings();
		jornal.clearArea();
		if (mySide == false)
			waitStep();
	}

	@Override
	public void sendStep(TData pack)
	{
		manager.send(pack);
		waitStep();
	}

	@Override
	public void gemeOver(boolean winner)
	{
		if (winner)
			JOptionPane.showMessageDialog(null, "Winner: WHITE!");
		else
			JOptionPane.showMessageDialog(null, "Winner: BLACK!");
		manager.close();
		this.dispose();
	}
}
