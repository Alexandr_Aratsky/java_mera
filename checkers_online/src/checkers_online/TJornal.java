package checkers_online;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;


public class TJornal extends JPanel
{
	private static final long	serialVersionUID	= 1L;
	private JTextField textField;
	private JTextArea txtArea;
	
	public TJornal(String title)
	{		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gbl_panel);
		
		textField = new JTextField(title);
		textField.setEditable(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 0;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		txtArea = new JTextArea();
		txtArea.setText("");
		GridBagConstraints gbc_txtArea = new GridBagConstraints();
		gbc_txtArea.fill = GridBagConstraints.BOTH;
		gbc_txtArea.gridx = 0;
		gbc_txtArea.gridy = 1;
		JScrollPane scroll = new JScrollPane (txtArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(scroll, gbc_txtArea);
		
	}

	public void setTitle(String title)
	{
		textField.setText(title);
	}
	public void appendString(String str)
	{
		try {
		      Document doc = txtArea.getDocument();
		      doc.insertString(doc.getLength(), str, null);
		   } catch(BadLocationException exc) { exc.printStackTrace(); }
	}
	public void clearArea()
	{
		txtArea.setText("");
	}
	
	
}
