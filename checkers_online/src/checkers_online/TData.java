package checkers_online;

import java.io.Serializable;
import java.util.ArrayDeque;

class Pair<K, V>
{

	private K	item1;
	private V	item2;

	public static <K, V> Pair<K, V> createPair(K item1, V item2)
	{
		return new Pair<K, V>(item1, item2);
	}

	public Pair(K item1, V item2)
	{
		this.item1 = item1;
		this.item2 = item2;
	}

	public K getFirst()
	{
		return item1;
	}

	public V getSecond()
	{
		return item2;
	}

	public void setItems(K item1, V item2)
	{
		this.item1 = item1;
		this.item2 = item2;
	}

}

public class TData implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4008310603009876416L;
	private ArrayDeque<TCell>	queue;

	public TData(ArrayDeque<TCell> queue)
	{
		this.queue = queue;
	}

	public ArrayDeque<TCell> getQueue()
	{
		return queue;
	}

}
