package checkers_online;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class TDialogFrame extends JFrame implements ISelectColor, IConnect
{

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 4264059042628099980L;
	private final JLabel			lblIPv4				= new JLabel("IP:");
	private final JLabel			lblPort				= new JLabel("Port:");
	private final JTextField		txtIPAdress;
	private final JTextField		txtPort;
	private final TSelectColor		panelSelect;
	private final JComboBox<String>	comboBox;
	private JLabel					lblSocketinfo;
	private TSocketManager			sManager;
	private JButton					okButton;

	@SuppressWarnings("unused")
	private void setBoardSizes(int selectedIndex,
			Pair<Integer, Integer> boardSize, Pair<Integer, Integer> panelSize)
	{
		switch (selectedIndex)
		{
			case 0:
				panelSize.setItems(530, 530);
				boardSize.setItems(8, 8);
				break;
			case 1:
				panelSize.setItems(480, 480);
				boardSize.setItems(6, 6);
				break;
			case 2:
				panelSize.setItems(600, 600);
				boardSize.setItems(12, 12);
				break;
			case 3:
				panelSize.setItems(630, 430);
				boardSize.setItems(12, 8);
				break;
			default:
				panelSize.setItems(TCell.defaultBoardSize * 50,
						TCell.defaultBoardSize * 50);
				boardSize.setItems(TCell.defaultBoardSize,
						TCell.defaultBoardSize);
				break;
		}
	}

	@SuppressWarnings("deprecation")
	void connect(boolean isserver)
	{
		if (isserver)
		{
			sManager = new TSocketManager(Integer.parseInt(txtPort.getText()),
					"127.0.0.1", true, this);
			lblSocketinfo.setText("Waiting client...");
			this.setCursor(Cursor.WAIT_CURSOR);
			panelSelect.setEnabled(false);
			okButton.setEnabled(false);
			(new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					sManager.startServer(comboBox.getSelectedIndex());

				}
			})).start();
		}
		else
		{
			sManager = new TSocketManager(Integer.parseInt(txtPort.getText()),
					txtIPAdress.getText(), false, this);
			lblSocketinfo.setText("Waiting server...");
			this.setCursor(Cursor.WAIT_CURSOR);
			panelSelect.setEnabled(false);
			okButton.setEnabled(false);
			(new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					sManager.startClient();

				}
			})).start();
		}

	}

	/**
	 * Create the dialog.
	 */
	public TDialogFrame()
	{
		setResizable(false);
		setTitle("Checkers 1.0 New");
		setBounds(100, 100, 396, 267);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 450, 0 };
		gridBagLayout.rowHeights = new int[] { 237, 35, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		getContentPane().add(panel, gbc_panel);
		panel.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("max(66dlu;default)"),
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"),
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("default:grow"), }));
		panel.add(lblIPv4, "2, 4, right, default");
		txtIPAdress = new JTextField();
		txtIPAdress.setEnabled(false);
		txtIPAdress.setText(TSocketManager.defaultIPv4);
		panel.add(txtIPAdress, "4, 4, 3, 1, fill, default");
		txtIPAdress.setColumns(10);
		panel.add(lblPort, "2, 6, right, default");
		txtPort = new JTextField();
		txtPort.setText(TSocketManager.defaultPort);
		panel.add(txtPort, "4, 6, fill, default");
		txtPort.setColumns(10);

		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Classic (8x8)", "Small (6x6)", "Big (12x12)", "Wide(12x8)" }));
		panel.add(comboBox, "6, 6, fill, default");
		panelSelect = new TSelectColor(this);
		panel.add(panelSelect, "4, 8, 3, 1, fill, fill");

		JPanel buttonPane = new JPanel();
		GridBagConstraints gbc_buttonPane = new GridBagConstraints();
		gbc_buttonPane.anchor = GridBagConstraints.NORTH;
		gbc_buttonPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_buttonPane.gridx = 0;
		gbc_buttonPane.gridy = 1;
		getContentPane().add(buttonPane, gbc_buttonPane);
		GridBagLayout gbl_buttonPane = new GridBagLayout();
		gbl_buttonPane.columnWidths = new int[] { 233, 70, 73, 0 };
		gbl_buttonPane.rowHeights = new int[] { 25, 0 };
		gbl_buttonPane.columnWeights = new double[] { 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_buttonPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		buttonPane.setLayout(gbl_buttonPane);

		JButton cancelButton = new JButton("Close");
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				dispose();
			}
		});

		okButton = new JButton("Start");
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				connect(panelSelect.getChoise());
			}
		});

		lblSocketinfo = new JLabel("SocketInfo");
		GridBagConstraints gbc_lblSocketinfo = new GridBagConstraints();
		gbc_lblSocketinfo.insets = new Insets(0, 0, 0, 5);
		gbc_lblSocketinfo.gridx = 0;
		gbc_lblSocketinfo.gridy = 0;
		buttonPane.add(lblSocketinfo, gbc_lblSocketinfo);
		okButton.setActionCommand("OK");
		GridBagConstraints gbc_okButton = new GridBagConstraints();
		gbc_okButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_okButton.insets = new Insets(0, 0, 0, 5);
		gbc_okButton.gridx = 1;
		gbc_okButton.gridy = 0;
		buttonPane.add(okButton, gbc_okButton);
		getRootPane().setDefaultButton(okButton);
		GridBagConstraints gbc_cancelButton = new GridBagConstraints();
		gbc_cancelButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_cancelButton.gridx = 2;
		gbc_cancelButton.gridy = 0;
		buttonPane.add(cancelButton, gbc_cancelButton);

	}

	@Override
	public void serverClicked()
	{
		comboBox.setEnabled(true);
		txtIPAdress.setEnabled(false);
	}

	@Override
	public void clientClicked()
	{
		comboBox.setEnabled(false);
		txtIPAdress.setEnabled(true);
	}

	@Override
	public void connected(String connectionInfo, int gameType)
	{
		lblSocketinfo.setText(connectionInfo);
		Pair<Integer, Integer> boardSize = Pair.createPair(-1, -1);
		Pair<Integer, Integer> panelSize = Pair.createPair(-1, -1);
		setBoardSizes(gameType, boardSize, panelSize);
		try
		{

			TMainWindow frame = new TMainWindow(boardSize, panelSize,
					panelSelect.getChoise(), sManager);
			frame.setVisible(true);
			this.dispose();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void someException(String message)
	{
		setCursor(Cursor.DEFAULT_CURSOR);
		JOptionPane.showMessageDialog(null, message);
		this.dispose();
	}
}
