package checkers_online;

import java.util.ArrayDeque;

interface IStepable
{

	void sendStep(TData pack);

	void gemeOver(boolean winner);
}

public class TBoard
{

	private TState[][]			states;
	private TFigure[][]			board;
	private final int			warLines		= 3 * TCell.getBoardHeight() / 8;
	private final int			checkersNumber	= TCell.getBoardWidth()
														* warLines / 2;
	private ArrayDeque<TCell>	movesQueue;
	private int					scoreWhite;
	private int					scoreBlack;
	private String				stepString;
	private static IStepable	owner;

	public static void setOwn(IStepable own)
	{
		owner = own;
	}

	private void createCheckers()
	{
		for (int j = 0; j < warLines; j++)
			for (int i = 0; i < checkersNumber / warLines; i++)
				board[2 * i % TCell.getBoardWidth() + ((j % 2 == 0) ? 1 : 0)][j] = TFigure.WHITE;

		for (int j = 0; j < warLines; j++)
			for (int i = 0; i < checkersNumber / warLines; i++)
				board[2 * i % TCell.getBoardWidth() + ((j % 2 == 0) ? 0 : 1)][TCell
						.getBoardHeight() - 1 - j] = TFigure.BLACK;
	}

	private void clearBoard()
	{
		for (int i = 0; i < TCell.getBoardWidth(); i++)
			for (int j = 0; j < TCell.getBoardHeight(); j++)
				board[i][j] = TFigure.EMPTY;
	}

	private void clearStates()
	{
		for (int i = 0; i < TCell.getBoardWidth(); i++)
			for (int j = 0; j < TCell.getBoardHeight(); j++)
				states[i][j] = TState.EMPTY;
	}

	public TBoard()
	{
		states = new TState[TCell.getBoardWidth()][TCell.getBoardHeight()];
		board = new TFigure[TCell.getBoardWidth()][TCell.getBoardHeight()];
		movesQueue = new ArrayDeque<TCell>();
		clearBoard();
		clearStates();
		createCheckers();
		TFigure.resetColor();
		scoreBlack = 0;
		scoreBlack = 0;
	}

	public TState getState(TCell cell)
	{
		return states[cell.X][cell.Y];
	}

	public TFigure getFugure(TCell cell)
	{
		return board[cell.X][cell.Y];
	}

	private void setSelect(TCell cell)
	{
		states[cell.X][cell.Y] = TState.SELECTED;
	}

	private void setCanMove(TCell cell)
	{
		states[cell.X][cell.Y] = TState.CAN_MOVE;
	}

	private void setCanKill(TCell cell)
	{
		states[cell.X][cell.Y] = TState.CAN_KILL;
	}

	private void setEmptyFigure(TCell cell)
	{
		board[cell.X][cell.Y] = TFigure.EMPTY;
	}

	public int getScoreWhite()
	{
		return scoreWhite;
	}

	public int getScoreBlack()
	{
		return scoreBlack;
	}

	public String getStepString()
	{
		return stepString;
	}

	private boolean setQweenLine(TCell cell, TMoveVector vector,
			boolean secondMove)
	{
		TCell tmp = new TCell(cell);
		TCell enemy = null;
		while (tmp.simpleMove(vector))
		{
			if (!TState.isEmpty(getState(tmp)))
				return false;
			if (TFigure.isFriend(getFugure(tmp)))
				break;
			if (TFigure.isEmpty(getFugure(tmp)))
			{
				if (!secondMove)
					setCanMove(tmp);
				else if (enemy != null)
					setCanMove(tmp);
				if (enemy != null)
					setCanKill(enemy);
				enemy = null;
			}
			if (TFigure.isEnemy(getFugure(tmp)))
				if (enemy != null)
					break;
				else
					enemy = new TCell(tmp);
		}
		return true;
	}

	private boolean setSimpleLine(TCell cell, TMoveVector vector)
	{
		TCell tmp = new TCell(cell);
		TCell enemy = null;
		int count = 1;
		while (tmp.simpleMove(vector) && count != 3)
		{
			if (!TState.isEmpty(getState(tmp)))
				return false;
			if (TFigure.isFriend(getFugure(tmp)))
				break;
			if (TFigure.isEmpty(getFugure(tmp)))
			{
				if (count == 1)
					break;
				else
				{
					setCanMove(tmp);
					setCanKill(enemy);
					break;
				}
			}
			if (TFigure.isEnemy(getFugure(tmp)))
				enemy = new TCell(tmp);
			count++;
		}
		return true;
	}

	private void killFigure(TCell cell)
	{
		if (TFigure.isEmpty(getFugure(cell)))
			return;
		else
		{
			if (TFigure.getFigureTeam(getFugure(cell)))
				scoreWhite++;
			else
				scoreBlack++;
			setEmptyFigure(cell);
		}
	}

	private void killLine(TCell c1, TCell c2)
	{
		if (TCell.cellsDistance(c1, c2) == -1)
			return;
		TCell vector = new TCell((c2.X - c1.X) / TCell.cellsDistance(c1, c2),
				(c2.Y - c1.Y) / TCell.cellsDistance(c1, c2));
		TCell tmp = new TCell(c1);
		while (!tmp.equels(c2))
		{
			tmp.add(vector);
			if (TFigure.isEnemy(getFugure(tmp)))
				killFigure(tmp);
		}
	}

	private boolean isKillLine(TCell cell, TMoveVector vector)
	{
		TCell tmp = new TCell(cell);
		TCell enemy = null;
		while (tmp.simpleMove(vector))
		{
			if (!TState.isEmpty(getState(tmp)))
				break;
			if (TFigure.isFriend(getFugure(tmp)))
				break;
			if (TFigure.isEmpty(getFugure(tmp)))
			{
				if (enemy != null)
					return true;
			}
			if (TFigure.isEnemy(getFugure(tmp)))
				if (enemy != null)
					break;
				else
					enemy = new TCell(tmp);
		}
		return false;

	}

	private boolean onKillLine(TCell c1, TCell c2)
	{
		if (TCell.cellsDistance(c1, c2) == -1)
			return false;
		TCell vector = new TCell((c2.X - c1.X) / TCell.cellsDistance(c1, c2),
				(c2.Y - c1.Y) / TCell.cellsDistance(c1, c2));
		TCell tmp = new TCell(c1);
		while (!tmp.equels(c2))
		{
			tmp.add(vector);
			if (TFigure.isEnemy(getFugure(tmp)))
				return true;
		}
		return false;
	}

	public void cellClick(TCell cell)
	{
		if (TState.isCanMove(getState(cell)))
		{
			if (!TFigure.isQween(getFugure(movesQueue.getFirst())))
			{

				if (movesQueue.size() == 1)
				{
					movesQueue.addLast(cell);
					setSelect(cell);
					if (TCell.cellsDistance(movesQueue.getFirst(),
							movesQueue.getLast()) == 2)
					{
						setSimpleLine(cell, TMoveVector.UP_RIGHT);
						setSimpleLine(cell, TMoveVector.DOWN_RIGHT);
						setSimpleLine(cell, TMoveVector.UP_LEFT);
						setSimpleLine(cell, TMoveVector.DOWN_LEFT);
					}
				}
				else
				{
					TCell c2 = movesQueue.removeLast();
					TCell c1 = movesQueue.removeLast();
					switch (TCell.cellsDistance(c1, c2))
					{
						case 1:
							movesQueue.addLast(c1);
							movesQueue.addLast(cell);
							setSelect(cell);
							setCanMove(c2);
							break;
						case 2:
							movesQueue.addLast(c1);
							movesQueue.addLast(c2);
							if (TCell.cellsDistance(new TCell(cell),
									movesQueue.getFirst()) != 1
									&& TCell.cellsDistance(new TCell(cell),
											movesQueue.getLast()) == 2)
							{
								movesQueue.addLast(new TCell(cell));
								setSelect(cell);
								setSimpleLine(cell, TMoveVector.UP_RIGHT);
								setSimpleLine(cell, TMoveVector.DOWN_RIGHT);
								setSimpleLine(cell, TMoveVector.UP_LEFT);
								setSimpleLine(cell, TMoveVector.DOWN_LEFT);
							}
							else
							{
								TCell tmp = movesQueue.getFirst();
								clearStates();
								movesQueue.clear();
								cellClick(tmp);
								cellClick(cell);
							}
							break;
						default:
							clearStates();
							movesQueue.clear();
							break;
					}
				}
			}
			else
			{
				if (movesQueue.size() == 1)
				{
					movesQueue.addLast(cell);
					setSelect(cell);
					if (onKillLine(movesQueue.getFirst(), movesQueue.getLast()))
					{
						if (isKillLine(cell, TMoveVector.UP_RIGHT))
							setQweenLine(cell, TMoveVector.UP_RIGHT, true);
						if (isKillLine(cell, TMoveVector.UP_LEFT))
							setQweenLine(cell, TMoveVector.UP_LEFT, true);
						if (isKillLine(cell, TMoveVector.DOWN_LEFT))
							setQweenLine(cell, TMoveVector.DOWN_LEFT, true);
						if (isKillLine(cell, TMoveVector.DOWN_RIGHT))
							setQweenLine(cell, TMoveVector.DOWN_RIGHT, true);
					}
				}
				else
				{
					TCell c2 = movesQueue.removeLast();
					TCell c1 = movesQueue.removeLast();
					if (!onKillLine(c1, c2)) // && !TCell.hasCellOnLine(cell,
												// c1, c2))
					{
						movesQueue.addLast(c1);
						movesQueue.addLast(cell);
						setSelect(cell);
						setCanMove(c2);
					}
					else
					{
						movesQueue.addLast(c1);
						movesQueue.addLast(c2);
						if (!TCell.hasCellOnLine(cell, c1, c2)
								&& onKillLine(c2, cell))
						{
							movesQueue.addLast(cell);
							setSelect(cell);
							if (isKillLine(cell, TMoveVector.UP_RIGHT))
								setQweenLine(cell, TMoveVector.UP_RIGHT, true);
							if (isKillLine(cell, TMoveVector.UP_LEFT))
								setQweenLine(cell, TMoveVector.UP_LEFT, true);
							if (isKillLine(cell, TMoveVector.DOWN_LEFT))
								setQweenLine(cell, TMoveVector.DOWN_LEFT, true);
							if (isKillLine(cell, TMoveVector.DOWN_RIGHT))
								setQweenLine(cell, TMoveVector.DOWN_RIGHT, true);
						}
						else
						{
							TCell tmp = movesQueue.getFirst();
							clearStates();
							movesQueue.clear();
							cellClick(tmp);
							cellClick(cell);
						}
					}
				}
			}
		}
		else
		{
			clearStates();
			movesQueue.clear();
			if (TFigure.isFriend(getFugure(cell)))
			{
				movesQueue.addFirst(cell);
				setSelect(cell);
				if (TFigure.isQween(getFugure(cell)))
				{
					setQweenLine(cell, TMoveVector.UP_RIGHT, false);
					setQweenLine(cell, TMoveVector.UP_LEFT, false);
					setQweenLine(cell, TMoveVector.DOWN_LEFT, false);
					setQweenLine(cell, TMoveVector.DOWN_RIGHT, false);
				}
				else
				{
					TCell turn_left = new TCell(cell);
					TCell turn_right = new TCell(cell);
					if (TFigure.getCurrentColorBoolean())
					{
						if (turn_left.simpleMove(TMoveVector.DOWN_LEFT)
								&& TFigure.isEmpty(getFugure(turn_left)))
							setCanMove(turn_left);
						if (turn_right.simpleMove(TMoveVector.DOWN_RIGHT)
								&& TFigure.isEmpty(getFugure(turn_right)))
							setCanMove(turn_right);
					}
					else

					{
						if (turn_left.simpleMove(TMoveVector.UP_LEFT)
								&& TFigure.isEmpty(getFugure(turn_left)))
							setCanMove(turn_left);
						if (turn_right.simpleMove(TMoveVector.UP_RIGHT)
								&& TFigure.isEmpty(getFugure(turn_right)))
							setCanMove(turn_right);
					}
					setSimpleLine(cell, TMoveVector.UP_RIGHT);
					setSimpleLine(cell, TMoveVector.DOWN_RIGHT);
					setSimpleLine(cell, TMoveVector.UP_LEFT);
					setSimpleLine(cell, TMoveVector.DOWN_LEFT);
				}
			}
			else
				return;
		}
	}

	public void giveUp()
	{
		clearStates();
		movesQueue.clear();
		owner.sendStep(new TData(movesQueue.clone()));
		if (TFigure.getCurrentColorBoolean())
			owner.gemeOver(false);
		else
			owner.gemeOver(true);
	}

	public boolean step(TData pack)
	{
		if (pack == null)
		{
			if (movesQueue.size() < 2)
				return false;
			owner.sendStep(new TData(movesQueue.clone()));
			TCell tmp1 = movesQueue.removeFirst();
			stepString = TCell.getCelllString(tmp1);
			while (!movesQueue.isEmpty())
			{
				TCell tmp2 = movesQueue.removeFirst();
				board[tmp2.X][tmp2.Y] = getFugure(tmp1);
				killLine(tmp1, tmp2);
				setEmptyFigure(tmp1);
				tmp1 = tmp2;
			}
			stepString = stepString + " -> " + TCell.getCelllString(tmp1);
			if (!TFigure.getFigureTeam(getFugure(tmp1)) && tmp1.Y == 0)
				board[tmp1.X][tmp1.Y] = TFigure.BLACK_QWEEN;
			if (TFigure.getFigureTeam(getFugure(tmp1))
					&& tmp1.Y == TCell.getBoardHeight() - 1)
				board[tmp1.X][tmp1.Y] = TFigure.WHITE_QWEEN;
			TFigure.nextColor();
			clearStates();
			movesQueue.clear();
			if (scoreWhite == checkersNumber)
				owner.gemeOver(false);
			else if (scoreBlack == checkersNumber)
				owner.gemeOver(true);
			return true;
		}
		else
		{
			ArrayDeque<TCell> queue = pack.getQueue();
			if (queue.size() == 0)
			{
				owner.gemeOver(!TFigure.getCurrentColorBoolean());
				clearStates();
				movesQueue.clear();
				return true;
			}
			TCell tmp1 = queue.removeFirst();
			stepString = TCell.getCelllString(tmp1);
			while (!queue.isEmpty())
			{
				TCell tmp2 = queue.removeFirst();
				board[tmp2.X][tmp2.Y] = getFugure(tmp1);
				killLine(tmp1, tmp2);
				setEmptyFigure(tmp1);
				tmp1 = tmp2;
			}
			stepString = stepString + " -> " + TCell.getCelllString(tmp1);
			if (!TFigure.getFigureTeam(getFugure(tmp1)) && tmp1.Y == 0)
				board[tmp1.X][tmp1.Y] = TFigure.BLACK_QWEEN;
			if (TFigure.getFigureTeam(getFugure(tmp1))
					&& tmp1.Y == TCell.getBoardHeight() - 1)
				board[tmp1.X][tmp1.Y] = TFigure.WHITE_QWEEN;
			TFigure.nextColor();
			clearStates();
			movesQueue.clear();
			if (scoreWhite == checkersNumber)
				owner.gemeOver(false);
			else if (scoreBlack == checkersNumber)
				owner.gemeOver(true);
			return true;

		}
	}
}
