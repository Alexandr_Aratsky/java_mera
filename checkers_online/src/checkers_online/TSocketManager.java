package checkers_online;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayDeque;

interface IConnect
{
	public void connected(String connectionInfo, int gameType);

	public void someException(String message);
}

class TSocketManager
{
	public static final String	defaultIPv4	= "127.0.0.1";
	public static final String	defaultPort	= "7777";

	private InetAddress			ipAddress;
	private int					portNumber;
	private ServerSocket		serverSocket;
	private Socket				socket;
	private boolean				iAmServer;
	private IConnect			connection;

	private ObjectInputStream	in;
	private ObjectOutputStream	out;

	public TSocketManager(int port, String ip, boolean server,
			IConnect connection)
	{
		iAmServer = server;
		portNumber = port;
		this.connection = connection;
		try
		{
			if (iAmServer)
			{
				serverSocket = new ServerSocket(portNumber);
				serverSocket.setSoTimeout(60000);
				ipAddress = serverSocket.getInetAddress();
			}
			else
			{
				ipAddress = InetAddress.getByName(ip);
			}
		}
		catch (IOException e)
		{
			connection.someException(e.getMessage());
		}
	}

	public void startServer(int gameType)
	{
		if (!iAmServer)
			(new Exception("it's client")).printStackTrace();
		try
		{
			socket = serverSocket.accept();
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(gameType);
		}
		catch (Exception e)
		{
			connection.someException(e.getMessage());
			return;
		}
		connection.connected("Connected", gameType);
	}

	public void startClient()
	{
		int gameType;
		if (iAmServer)
			(new Exception("it's server")).printStackTrace();
		try
		{
			socket = new Socket(ipAddress, portNumber);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			gameType = (Integer) in.readObject();
		}
		catch (Exception e)
		{
			connection.someException(e.getMessage());
			return;
		}
		connection.connected("Connected", gameType);
	}

	public void send(TData pack)
	{
		try
		{
			out.writeObject(pack.getQueue());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public TData receive()
	{
		TData res = null;
		try
		{
			res = new TData((ArrayDeque<TCell>) in.readObject());
		}
		catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@SuppressWarnings("static-access")
	public String getIPAddress()
	{
		try
		{
			return ipAddress.getLocalHost().getHostAddress();
		}
		catch (UnknownHostException e)
		{
			return "[UnknownHostException]";
		}
	}

	public void close()
	{
		try
		{
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
}
