package checkers_online;

import java.awt.EventQueue;

import javax.swing.JDialog;

public class TStartPoint
{
	public static int gameType;
	public static boolean side;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					TDialogFrame dialog = new TDialogFrame();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
					
				} catch (Exception e) { e.printStackTrace(); }
			}
		});
	}

}
