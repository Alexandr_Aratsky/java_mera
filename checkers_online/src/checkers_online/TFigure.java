package checkers_online;

public enum TFigure 
{
	EMPTY, WHITE, BLACK,  WHITE_QWEEN, BLACK_QWEEN;
	
	// текущий цвет (истина - белые, ложь - чёрные)
	private static boolean currentColor = true;
	
	public static String getCurrentColorString()
	{
		if (currentColor) return "WHITE";
		else return "BLACK";
	}
	public static boolean getCurrentColorBoolean()
	{
		return currentColor;
	}
	public static void nextColor()
	{
		currentColor = !currentColor;
	}
	public static void resetColor()
	{
		currentColor = true;
	}
	public static boolean isFriend(TFigure figure)
	{
		if (currentColor)
			if (figure == WHITE || figure == WHITE_QWEEN) return true;
			else return false;
		else 
			if (figure == BLACK|| figure == BLACK_QWEEN) return true;
			else return false;		
	}
	public static boolean isEnemy(TFigure figure)
	{
		if (!currentColor)
			if (figure == WHITE || figure == WHITE_QWEEN) return true;
			else return false;
		else 
			if (figure == BLACK|| figure == BLACK_QWEEN) return true;
			else return false;		
	}
	public static boolean isQween(TFigure figure)
	{
		if (figure == TFigure.WHITE_QWEEN || figure == TFigure.BLACK_QWEEN) return true;
		else return false;
	}
	public static boolean isEmpty(TFigure figure)
	{
		if (figure == TFigure.EMPTY) return true;
		return false;
	}
	public static boolean getFigureTeam(TFigure figure)
	{
		switch (figure) 
		{
			case BLACK: case BLACK_QWEEN: return false;
			case WHITE: case WHITE_QWEEN: return true;	
			default:return false;
		}
	}
}
